package aiai;

/**
 * @author 200217AM
 *
 */
/**
 * @author 200217AM
 *
 */
public class Main {

	/**
	 * アイアイを表示するプログラム
	 *
	 * @return
	 */
	public static String sayAiai() {
		return "アイアイ(アイアイ)";
	}

	/**
	 * @param msg
	 * @return
	 */
	public static String repeatMessage(String msg, int count){
		String repeatMsg="";
		for(int i=0; i<count; i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**
	 * 改行する
	 *
	 * @return
	 */
	public static String newLine() {
		return "\n";
	}

	/**
	 * おさるさんだよを表示
	 *
	 * @return
	 */
	public static String sayMonkey() {
		return "おさるさんだよ";
	}

	/**みなみのしまのを表示
	 * @return
	 */
	public static String saySouth() {
		return "みなみのしまの";
	}

	/**しっぽのながいを表示
	 * @return
	 */
	public static String sayLongTail(){
		return "しっぽのながい";
	}

	/**おさるさんだねを表示
	 * @return
	 */
	public static String saySandane(){
		return "おさるさんだね";
	}



	/**きのはのおうちを表示
	 * @return
	 */
	public static String kinohaHouse(){
		return "きのはのおうち";
	}



	/**おめめのまいを表示
	 * @return
	 */
	public static String eyeDance(){
		return "おめめのまるい";
	}


	/**あいあいをうたう処理
	 *
	 */
	public static void execute(){
		String liric="";
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=sayMonkey();
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=saySouth();
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=sayLongTail();
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=sayMonkey();

		//ここから二番
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=saySandane();
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=kinohaHouse();
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=eyeDance();
		liric+=newLine();
		liric+=repeatMessage(sayAiai(),2);
		liric+=newLine();
		liric+=saySandane();
		liric+=newLine();

		//以降、自作のメソッドを使ってアイアイを完成させてください。

		System.out.println(liric);
	}







	/**メインメソッド
	 * @param args
	 */
	public static void main(String[] args) {
		execute();

	}








}
